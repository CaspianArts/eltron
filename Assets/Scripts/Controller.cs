﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Controller : MonoBehaviour {

    //assume 0, 0, 0 origin

    public int gridSize = 10;
    public int cubeSize = 10;
    public int padding = 2;

    private GameObject mainCamera;

    private int universeTick = 0;

    private ArrayList negatronLines = new ArrayList();
    private ArrayList negatronSperes = new ArrayList();

    void Awake()
    {
    }

    // Use this for initialization
    void Start () 
    {
        mainCamera = GameObject.Find("MainCamera");
        if(mainCamera != null)
        {
            //move from origin z to -gridSize z
            //mainCamera.transform.position = new Vector3(0, 0, startRange - 162);
        }

        //lightwave test (bootstrap)
        //mid y cube, starting at negative most x

        /*
        //Standalone...      

        int x = 0;
        int y = 0;
        int z = 0;

        ElTron elTron;
        elTrons.TryGetValue(new IntVector3(x, y, z), out elTron);
        //move in x+ at light speed (delay = 0)
        elTron.dimensionX.direction = Direction.positive;
        elTron.dimensionX.delay = 0;
        elTron.debugName = "x-test";
        elTron.debugColor = Color.red;
        elTron.debugNegatronColor = Color.blue;
              
        visualize(elTron);
        */

        //Only two...
        //horizontal wave
        int x = 0;
        int y = 0;
        int z = 0;
        PlanckContainer planckContainer = new PlanckContainer(x, y, z);
        ElTron elTron = new ElTron();
        planckContainer.currentElTrons.Add(elTron);
        PlanckContainer.liveContainers.Add(planckContainer.getPlanckIndex(), planckContainer);

        //move in x+ at light speed (delay = 0)
        elTron.dimensionX.direction = Direction.positive;
        elTron.dimensionX.delay = 0;
        elTron.dimensionX.debugName = "x forward of h wave component";
        elTron.dimensionZ.direction = Direction.positive;
        elTron.dimensionZ.delay = 1;
        elTron.dimensionZ.debugName = "horizontal wave component";
        elTron.debugName = "horizontal wave";
        elTron.debugNegatronColor = Color.red;
        elTron.debugColor = Color.red;

        visualize(planckContainer);

        //vertical wave
        x = 0;
        y = 0;
        z = 0;

        elTron = new ElTron();
        planckContainer.currentElTrons.Add(elTron);

        //move in y+ at 1/2 light speed (delay = 1)
        elTron.dimensionX.direction = Direction.positive;
        elTron.dimensionX.delay = 0;
        elTron.dimensionX.debugName = "x forward of v wave component";
        elTron.dimensionY.direction = Direction.positive;
        elTron.dimensionY.delay = 1;
        elTron.dimensionY.debugName = "vertical wave component";
        elTron.debugName = "vertical wave";
        elTron.debugNegatronColor = Color.green;
        elTron.debugColor = Color.green;

        visualize(planckContainer);

        /* two with leader
        //z-follower
        int x = 0;
        int y = 0;
        int z = 1;

        ElTron elTron;
        elTrons.TryGetValue(new IntVector3(x, y, z), out elTron);
        //move in x+ at light speed (delay = 0)
        elTron.dimensionX.direction = Direction.positive;
        elTron.dimensionX.delay = 0;
        elTron.dimensionZ.direction = Direction.positive;
        elTron.dimensionZ.delay = 1;
        elTron.dimensionZ.debugName = "z-component";
        elTron.debugName = "z-follower";
        elTron.debugNegatronColor = Color.blue;

        elTron.gameObjectRef = GameObject.CreatePrimitive(PrimitiveType.Cube);
        int xPos = (x * cubeSize) + (x * padding);
        int yPos = (y * cubeSize) + (y * padding);
        int zPos = (z * cubeSize) + (z * padding);
        elTron.gameObjectRef.transform.position = new Vector3(xPos, yPos, zPos);
        elTron.gameObjectRef.transform.localScale = new Vector3(cubeSize, cubeSize, cubeSize);

        colorCube(elTron, Color.red, 1.0f);

        //y-follower
        x = 0;
        y = 1;
        z = 0;

        xPos = (x * cubeSize) + (x * padding);
        yPos = (y * cubeSize) + (y * padding);
        zPos = (z * cubeSize) + (z * padding);

        elTrons.TryGetValue(new IntVector3(x, y, z), out elTron);
        //move in y+ at 1/2 light speed (delay = 1)
        elTron.dimensionX.direction = Direction.positive;
        elTron.dimensionX.delay = 0;
        elTron.dimensionY.direction = Direction.positive;
        elTron.dimensionY.delay = 1;
        elTron.dimensionY.debugName = "y-component";
        elTron.debugName = "y-follower";
        elTron.debugNegatronColor = Color.green;

        elTron.gameObjectRef = GameObject.CreatePrimitive(PrimitiveType.Cube);
        elTron.gameObjectRef.transform.position = new Vector3(xPos, yPos, zPos);
        elTron.gameObjectRef.transform.localScale = new Vector3(cubeSize, cubeSize, cubeSize);

        colorCube(elTron, Color.red, 1.0f);

        //leader
        x = 3;
        y = 0;
        z = 0;

        xPos = (x * cubeSize) + (x * padding);
        yPos = (y * cubeSize) + (y * padding);
        zPos = (z * cubeSize) + (z * padding);

        elTrons.TryGetValue(new IntVector3(x, y, z), out elTron);
        //move in y+ at 1/2 light speed (delay = 1)
        elTron.dimensionX.direction = Direction.positive;
        elTron.dimensionX.delay = 0;
        elTron.dimensionX.debugName = "x-component";
        elTron.debugName = "x-leader";
        elTron.debugNegatronColor = Color.green;

        elTron.gameObjectRef = GameObject.CreatePrimitive(PrimitiveType.Cube);
        elTron.gameObjectRef.transform.position = new Vector3(xPos, yPos, zPos);
        elTron.gameObjectRef.transform.localScale = new Vector3(cubeSize, cubeSize, cubeSize);

        colorCube(elTron, Color.red, 1.0f);
        */
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0) == true)
        {
            cleanVisualizer();

            //copy containers to avoid enumeration sync issues
            List<PlanckContainer> containers = new List<PlanckContainer>();
            foreach (PlanckContainer container in PlanckContainer.liveContainers.Values)
            {
                containers.Add(container);
            }

            foreach (PlanckContainer container in containers)
            {
                container.universeTick = universeTick;
                //may update the live set with an ElTron
                container.moveNegatrons();
            }

            foreach (PlanckContainer container in PlanckContainer.liveContainers.Values)
            {
                container.updateNegatrons();
            }

            containers = new List<PlanckContainer>();
            foreach (PlanckContainer container in PlanckContainer.liveContainers.Values)
            {
                containers.Add(container);
            }

            foreach (PlanckContainer container in containers)
            {
                //this may add negatrons to the live container set
                container.moveElTrons();
            }

            foreach (PlanckContainer container in PlanckContainer.liveContainers.Values)
            {
                container.updateElTrons();
            }

            //ensure that we run over the 'live' set, not the copied
            foreach (PlanckContainer container in PlanckContainer.liveContainers.Values)
            {
                container.computeNegatronEffect();
            }

            //visualize and find containers that no longer contain either an ElTron or a Negatron 
            List<PlanckIndex> collectContainers = new List<PlanckIndex>();
            foreach (PlanckContainer container in PlanckContainer.liveContainers.Values)
            {
                visualize(container);

                //find containers to collect
                if (container.currentElTrons.Count == 0 &&
                    container.currentNegatrons.Count == 0)
                {
                    collectContainers.Add(container.getPlanckIndex());
                }
            }

            //remove all containers not containing an ElTron
            foreach(PlanckIndex planckIndex in collectContainers)
            {
                PlanckContainer.liveContainers.Remove(planckIndex);
            }

            universeTick++;
        }
	}

    void cleanVisualizer()
    {
        foreach (GameObject line in negatronLines)
        {
            Destroy(line, 0);
        }
        negatronLines.Clear();

        foreach (GameObject sphere in negatronSperes)
        {
            Destroy(sphere, 0);
        }
        negatronSperes.Clear();
    }

    void visualize(PlanckContainer container)
    {
        int x = container.getPlanckIndex().x;
        int y = container.getPlanckIndex().y;
        int z = container.getPlanckIndex().z;

        int xPos = (x * cubeSize) + (x * padding);
        int yPos = (y * cubeSize) + (y * padding);
        int zPos = (z * cubeSize) + (z * padding);

        Vector3 position = new Vector3(xPos, yPos, zPos);

        foreach (ElTron elTron in container.currentElTrons)
        {
            elTron.gameObjectRef = GameObject.CreatePrimitive(PrimitiveType.Cube);
            elTron.gameObjectRef.transform.position = position;
            elTron.gameObjectRef.transform.localScale = new Vector3(cubeSize, cubeSize, cubeSize);

            MeshRenderer r = elTron.gameObjectRef.GetComponent<MeshRenderer>();

            if (r != null)
            {
                r.material.color = elTron.debugColor;
            }

            r.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
            r.receiveShadows = false;
            r.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

            //transparent mode
            /*
            r.material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
            r.material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            r.material.SetInt("_ZWrite", 0);
            r.material.DisableKeyword("_ALPHATEST_ON");
            r.material.DisableKeyword("_ALPHABLEND_ON");
            r.material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
            r.material.renderQueue = 3000;
            */
        }

        float nsBaseOffset = 0.5f;
        float nsOffset = nsBaseOffset;
        float lineSize = 3f;
        foreach (Negatron negatron in container.currentNegatrons)
        {
            Vector3 nsPosition = new Vector3(position.x + nsOffset, position.y + cubeSize / 2 + nsBaseOffset, position.z);

            nsOffset += 0.5f;

            GameObject negatronSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            negatronSphere.transform.position = nsPosition;
            negatronSphere.transform.localScale = new Vector3(1f, 1f, 1f);
            MeshRenderer nsr = negatronSphere.GetComponent<MeshRenderer>();
            nsr.material.color = negatron.debugColor;
            negatronSperes.Add(negatronSphere);

            //color = negatron.debugColor;
            if (negatron.directionX == Direction.negative)
            {
                Vector3 end = nsPosition;
                end.x -= lineSize;
                drawLine(nsPosition, end, Color.red);
            }
            else if (negatron.directionX == Direction.positive)
            {
                Vector3 end = nsPosition;
                end.x += lineSize;
                drawLine(nsPosition, end, Color.red);
            }

            if (negatron.directionY == Direction.negative)
            {
                Vector3 end = nsPosition;
                end.y -= lineSize;
                drawLine(nsPosition, end, Color.green);
            }
            else if (negatron.directionY == Direction.positive)
            {
                Vector3 end = nsPosition;
                end.y += lineSize;
                drawLine(nsPosition, end, Color.green);
            }

            if (negatron.directionZ == Direction.negative)
            {
                Vector3 end = nsPosition;
                end.z -= lineSize;
                drawLine(nsPosition, end, Color.yellow);
            }
            else if (negatron.directionZ == Direction.positive)
            {
                Vector3 end = nsPosition;
                end.z += lineSize;
                drawLine(nsPosition, end, Color.yellow);
            }
        }
    }

    void drawLine(Vector3 start, Vector3 end, Color color)
    {
        GameObject line = new GameObject();

        negatronLines.Add(line);

        line.transform.position = start;
        line.AddComponent<LineRenderer>();
        LineRenderer lr = line.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.startColor = color;
        lr.endColor = color;
        lr.startWidth = 0.1f;
        lr.endWidth = 0.1f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
    }
}
