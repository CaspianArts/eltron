﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum Direction
{
    none = 0,
    positive = 1,
    negative = -1
}

public class PlanckIndex
{
    public int x;
    public int y;
    public int z;

    public PlanckIndex(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}

public class PlanckIndexComparer : IEqualityComparer<PlanckIndex>
{
    public bool Equals(PlanckIndex source, PlanckIndex test)
    {
        return (source.x == test.x && source.y == test.y && source.z == test.z);
    }

    public int GetHashCode(PlanckIndex source)
    {
        return (source.x.ToString() + source.y.ToString() + source.z.ToString()).GetHashCode();
    }
}

public class PlanckContainer
{
    private PlanckIndex planckIndex = null;
    public long universeTick = 0;

    public PlanckContainer(int x, int y, int z)
    {
        planckIndex = new PlanckIndex(x, y, z);
    }

    public PlanckContainer(PlanckIndex planckIndex)
    {
        this.planckIndex = planckIndex;
    }

    public PlanckIndex getPlanckIndex()
    {
        return planckIndex;
    }

    //the shared static live containers is a way to maintain system memory by only maintaining 
    //containers that are currently being used by the system to either hold
    //a negatron or an ElTron...or or both
    public static Dictionary<PlanckIndex, PlanckContainer> liveContainers = new Dictionary<PlanckIndex, PlanckContainer>(new PlanckIndexComparer());

    public List<ElTron> nextElTrons = new List<ElTron>();
    public List<ElTron> currentElTrons = new List<ElTron>();

    public List<Negatron> nextNegatrons = new List<Negatron>();
    public List<Negatron> currentNegatrons = new List<Negatron>();

    public void computeNegatronEffect()
    {
        if (currentElTrons.Count > 1)
        {
            Debug.Log("*********** " + currentElTrons.Count);
        }

        foreach (ElTron elTron in currentElTrons)
        {
            foreach (Negatron negatron in currentNegatrons)
            {
                //process X component
                computeNegatronEffect(elTron.dimensionX, negatron.directionX);
                computeNegatronEffect(elTron.dimensionY, negatron.directionY);
                computeNegatronEffect(elTron.dimensionZ, negatron.directionZ);
            }
        }
    }

    private void computeNegatronEffect(DimensionComponent dimensionComponent, Direction negatronDirection)
    { 
        //for now only components with direction are affected and are going less than 
        //the speed of light
        if (dimensionComponent.direction != Direction.none && negatronDirection != Direction.none && dimensionComponent.delay > 0)
        {
            if (dimensionComponent.direction != negatronDirection)
            {
                //decrease delay if negatron is going in different direction as motion
                //-->speed up
                dimensionComponent.accumulatedDelay--;

                Debug.Log(string.Format("Negatron {0} is in opposite direction, DECREMENT accumulated delay ({3}, {4}, {5}), D {1}, A {2} ", dimensionComponent.debugName, dimensionComponent.delay, dimensionComponent.accumulatedDelay, planckIndex.x, planckIndex.y, planckIndex.z));

            }
            else
            {
                //same direction
                //-->slow down
                dimensionComponent.accumulatedDelay++;

                Debug.Log(string.Format("Negatron {0} is in same direction, INCREMENT accumulated delay ({3}, {4}, {5}), D {1}, A {2} ", dimensionComponent.debugName, dimensionComponent.delay, dimensionComponent.accumulatedDelay, planckIndex.x, planckIndex.y, planckIndex.z));
            }
        }
    }

    //iterate through all ElTrons's and compute the effect
    //of negatrons on their components
    public void moveElTrons()
    {
        //use this as a temp container to hold leaving ElTrons
        //and then remove from our incoming list
        List<ElTron> movingElTrons = new List<ElTron>();

        foreach (ElTron elTron in currentElTrons)
        {
            int nextX = planckIndex.x;
            int nextY = planckIndex.y;
            int nextZ = planckIndex.z;

            bool xIsMoving = isComponentMoving(elTron.dimensionX);
            bool yIsMoving = isComponentMoving(elTron.dimensionY);
            bool zIsMoving = isComponentMoving(elTron.dimensionZ);

            if (xIsMoving == true)
            {
                nextX = planckIndex.x + 1;
                if (elTron.dimensionX.direction == Direction.negative)
                {
                    nextX = planckIndex.x - 1;
                }
                elTron.dimensionX.delayStartTick = universeTick + 1;
            }

            if (yIsMoving == true)
            {
                nextY = planckIndex.y + 1;
                if (elTron.dimensionY.direction == Direction.negative)
                {
                    nextY = planckIndex.y - 1;
                }
                elTron.dimensionY.delayStartTick = universeTick + 1;
            }

            if (zIsMoving == true)
            {
                nextZ = planckIndex.z + 1;
                if (elTron.dimensionZ.direction == Direction.negative)
                {
                    nextZ = planckIndex.z - 1;
                }
                elTron.dimensionZ.delayStartTick = universeTick + 1;
            }

            if (xIsMoving == true || yIsMoving == true || zIsMoving == true)
            {
                //look up adjacent ElTron where this ElTron is moving to...
                PlanckContainer nextContainer;
                PlanckIndex nextPlanckIndex = new PlanckIndex(nextX, nextY, nextZ);
                liveContainers.TryGetValue(nextPlanckIndex, out nextContainer);

                if (nextContainer == null)
                {
                    nextContainer = new PlanckContainer(nextPlanckIndex);
                    liveContainers.Add(nextPlanckIndex, nextContainer);
                }

                nextContainer.nextElTrons.Add(elTron);
                movingElTrons.Add(elTron);
            }
        }

        //removing all outgoing ElTrons
        foreach(ElTron elTron in movingElTrons)
        {
            currentElTrons.Remove(elTron);
        }
    }

    public void updateNegatrons()
    {
        //optimize using swap...
        currentNegatrons.Clear();
        List<Negatron> temp = currentNegatrons;
        currentNegatrons = nextNegatrons;
        nextNegatrons = temp;
    }

    public void updateElTrons()
    {
        //update ElTron state
        foreach (ElTron elTron in nextElTrons)
        {
            currentElTrons.Add(elTron);
        }
        nextElTrons.Clear();
    }

    private bool isComponentMoving(DimensionComponent dimensionComponent)
    {
        bool componentMoving = false;

        if (dimensionComponent.direction != Direction.none)
        {

            long waitTicks = universeTick - dimensionComponent.delayStartTick;
            if (componentMoving == false && waitTicks >= dimensionComponent.delay)
            {
                componentMoving = true;

                //apply any accumulated delay
                dimensionComponent.delay += dimensionComponent.accumulatedDelay;

                //filter out speed of light movement for now
                if (dimensionComponent.delay > 0)
                {
                    Debug.Log(String.Format("Is Moving ... delay updated to {0} and with AD {1}", dimensionComponent.delay, dimensionComponent.accumulatedDelay));
                }

                //check for max delay here (special case)
                if (dimensionComponent.delay >= DimensionComponent.MAX_DELAY)
                {
                    //do not reset the accumulated delay...this acts as speed control as the
                    //ElTron begins its movement in the opposite direction

                    dimensionComponent.delay -= DimensionComponent.MAX_DELAY;
                    Debug.Log(string.Format("Negatron is causing the component to flip {0}", dimensionComponent.debugName));
                    Debug.Log(String.Format("D{0},A{1}", dimensionComponent.delay, dimensionComponent.accumulatedDelay));

                    //flip direction
                    if (dimensionComponent.direction == Direction.negative)
                    {
                        dimensionComponent.direction = Direction.positive;
                    }
                    else
                    {
                        dimensionComponent.direction = Direction.negative;
                    }
                }

                //0 floor?
                dimensionComponent.delay = Math.Max(0, dimensionComponent.delay);

                dimensionComponent.accumulatedDelay = 0;
            }
        }
        return componentMoving;
    }

    //push negatrons to adjacent containers...
    public void moveNegatrons()
    {
        //only push out negatrons if the container has at least one ElTron
        if (currentElTrons.Count > 0)
        {
            for (int nx = -1; nx < 2; nx++)
            {
                for (int ny = -1; ny < 2; ny++)
                {
                    for (int nz = -1; nz < 2; nz++)
                    {
                        int ax = planckIndex.x + nx;
                        int ay = planckIndex.y + ny;
                        int az = planckIndex.z + nz;

                        //exclude self
                        if ((nx == 0 && ny == 0 && nz == 0) == false)
                        {
                            //to conserve system memory...only create PlanckContainers
                            //when necessary...and remove them when empty of 
                            //any ElTron or Negatron

                            PlanckIndex adjacentPlanckIndex = new PlanckIndex(ax, ay, az);
                            PlanckContainer adjacentContainer;
                            liveContainers.TryGetValue(adjacentPlanckIndex, out adjacentContainer);

                            if (adjacentContainer == null)
                            {
                                adjacentContainer = new PlanckContainer(adjacentPlanckIndex);
                                liveContainers.Add(adjacentPlanckIndex, adjacentContainer);
                            }

                            Negatron negatron = new Negatron();
                            //not the best, but use the first ElTron's debug color
                            negatron.debugColor = currentElTrons[0].debugNegatronColor;

                            //can only ever be -1, 0, or 1
                            int dx = adjacentContainer.planckIndex.x - planckIndex.x;
                            int dy = adjacentContainer.planckIndex.y - planckIndex.y;
                            int dz = adjacentContainer.planckIndex.z - planckIndex.z;

                            //the following determines to trajectory of the negatron
                            //base on this ElTron's position and the current
                            //adjacent neighbor to which it is being pushed
                            if (dx == -1)
                            {
                                negatron.directionX = Direction.negative;
                            }
                            else if (dx == 0)
                            {
                                negatron.directionX = Direction.none;
                            }
                            else if (dx == 1)
                            {
                                negatron.directionX = Direction.positive;
                            }

                            if (dy == -1)
                            {
                                negatron.directionY = Direction.negative;
                            }
                            else if (dy == 0)
                            {
                                negatron.directionY = Direction.none;
                            }
                            else if (dy == 1)
                            {
                                negatron.directionY = Direction.positive;
                            }

                            if (dz == -1)
                            {
                                negatron.directionZ = Direction.negative;
                            }
                            else if (dz == 0)
                            {
                                negatron.directionZ = Direction.none;
                            }
                            else if (dz == 1)
                            {
                                negatron.directionZ = Direction.positive;
                            }

                            adjacentContainer.nextNegatrons.Add(negatron);
                        }
                    }
                }
            }
        }

        foreach(Negatron negatron in currentNegatrons)
        {
            //find the correct adjacent neighbor based on the 
            //negatron's direction
            int ax = planckIndex.x;
            int ay = planckIndex.y;
            int az = planckIndex.z;
            if (negatron.directionX == Direction.negative)
            {
                ax = planckIndex.x - 1;
            }
            else if (negatron.directionX == Direction.positive)
            {
                ax = planckIndex.x + 1;
            }

            if (negatron.directionY == Direction.negative)
            {
                ay = planckIndex.y - 1;
            }
            else if (negatron.directionY == Direction.positive)
            {
                ay = planckIndex.y + 1;
            }

            if (negatron.directionZ == Direction.negative)
            {
                az = planckIndex.z - 1;
            }
            else if (negatron.directionZ == Direction.positive)
            {
                az = planckIndex.z + 1;
            }

            PlanckIndex adjacentPlanckIndex = new PlanckIndex(ax, ay, az);
            PlanckContainer adjacentContainer;
            liveContainers.TryGetValue(adjacentPlanckIndex, out adjacentContainer);

            if (adjacentContainer == null)
            {
                adjacentContainer = new PlanckContainer(adjacentPlanckIndex);
                liveContainers.Add(adjacentPlanckIndex, adjacentContainer);
            }
            adjacentContainer.nextNegatrons.Add(negatron);
        }
    }
}

public class Negatron
{
    public Direction directionX = Direction.none;
    public Direction directionY = Direction.none;
    public Direction directionZ = Direction.none;

    public PlanckIndex sourceIndex;
    //public String debugEltronName;
    public Color debugColor;
}

public class DimensionComponent
{
    public static long MAX_DELAY = 4;

    public Guid id = Guid.NewGuid();

    public Direction direction = Direction.none;

    //negatron either adds or removes accumulated delay and this is the actual delay of the ElTron face
    public long delay = 0;
    public long accumulatedDelay = 0;
    public long delayStartTick;

    public String debugName = "";
}

public class ElTron
{
    public Guid elTronId = Guid.NewGuid();
    public String debugName = "";
    public Color debugColor = Color.clear;
    public Color debugNegatronColor = Color.clear;

    public GameObject gameObjectRef;

    public DimensionComponent dimensionX = new DimensionComponent();
    public DimensionComponent dimensionY = new DimensionComponent();
    public DimensionComponent dimensionZ = new DimensionComponent();
}
